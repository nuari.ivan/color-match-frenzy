using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shapes : Item
{
    [SerializeField] private CharacterColor currentColor = CharacterColor.Red;
    [SerializeField] private ItemSkin[] allSkins;
    [SerializeField] private GameObject[] allFx;
    [SerializeField] private GameObject[] allFailedFx;

    private SpriteRenderer rend;

    private void OnEnable()
    {
        rend = GetComponentInChildren<SpriteRenderer>();

        int warna = Random.Range(0, allSkins.Length);
        int bentuk = Random.Range(0, allSkins[0].allSkins.Length);
        if(warna == 0)
        {
            currentColor = CharacterColor.Red;
        }
        if(warna == 1)
        {
            currentColor = CharacterColor.Yellow;
        }
        if(warna == 2)
        {
            currentColor = CharacterColor.Blue;
        }

        rend.sprite = allSkins[warna].allSkins[bentuk];
    }

    protected override void OnDetectPlayer(GameObject player)
    {
        CharacterMovement chara = player.GetComponent<CharacterMovement>();
        if (chara.GetCurrentColor() == currentColor)
        {
            GameSetting.Instance.AddScore();
            AudioManager.Instance.PlaySound("Success");
            GameObject tempFx = Instantiate(allFx[Random.Range(0, allFx.Length)], transform.position, Quaternion.identity);
            Destroy(tempFx, 1.5f);
        }
        else
        {
            GameSetting.Instance.GetHit();
            AudioManager.Instance.PlaySound("Failed");
            GameObject tempFx = Instantiate(allFailedFx[Random.Range(0, allFx.Length)], transform.position, Quaternion.identity);
            Destroy(tempFx, 1.5f);
        }
        Destroy(gameObject);
    }

    protected override void OnDetectGround()
    {
        GameSetting.Instance.GetHit();
        AudioManager.Instance.PlaySound("Failed");
        GameObject tempFx = Instantiate(allFailedFx[Random.Range(0, allFx.Length)], transform.position, Quaternion.identity);
        Destroy(tempFx, 1.5f);
        Destroy(gameObject);
    }
}
