using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private CharacterColor currentColor = CharacterColor.Red;
    [SerializeField] private CharacterSkin[] allskin;

    [SerializeField] private SpriteRenderer body;
    [SerializeField] private SpriteRenderer rightHand;
    [SerializeField] private SpriteRenderer lefthand;

    [SerializeField] private GameObject fx;

    private float inputX;
    private Vector2 movement;
    private int currentColorIndex = 0;

    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        GameSetting.Instance.OnPowerUpActived += Instance_PowerUp;
    }

    private void OnDisable()
    {
        GameSetting.Instance.OnPowerUpActived -= Instance_PowerUp;
    }

    private void Instance_PowerUp(int index)
    {
        if(index == 0)
        {
            speed *= 2;
            StartCoroutine(ReturnSpeed());
        }
        if (index == 1)
        {
            GameSetting.Instance.PowerUpDoubleBonus();
        }
        if (index == 2)
        {
            GameSetting.Instance.AddHealth();
        }
    }

    IEnumerator ReturnSpeed()
    {
        yield return new WaitForSeconds(5f);
        speed /= 2;
    }

    private void Update()
    {
        inputX = Input.GetAxisRaw("Horizontal");
        movement = new Vector2(inputX * speed, rb.velocity.y);

        if(inputX == 1)
        {
            transform.localRotation = Quaternion.Euler(0f, 0f, -10f);
        }
        if(inputX == -1)
        {
            transform.localRotation = Quaternion.Euler(0f, 0f, 10f);
        }
        if(inputX == 0)
        {
            transform.localRotation = Quaternion.identity;
        }

        if(Input.GetKeyDown(KeyCode.Space))
        {
            ChangeColor();
            GameObject tempFx = Instantiate(fx, transform);
            Destroy(tempFx, 1.5f);
        }
    }

    private void ChangeColor()
    {
        currentColorIndex++;
        if(currentColorIndex > 2)
        {
            currentColorIndex = 0;
        }

        if(currentColorIndex == 0)
        {
            currentColor = CharacterColor.Red;
            InitCharacter(0);
        }
        if(currentColorIndex == 1)
        {
            currentColor = CharacterColor.Yellow;
            InitCharacter(1);
        }
        if(currentColorIndex == 2)
        {
            currentColor = CharacterColor.Blue;
            InitCharacter(2);
        }
    }

    void InitCharacter(int index)
    {
        body.sprite = allskin[index].body;
        rightHand.sprite = allskin[index].rightHand;
        lefthand.sprite = allskin[index].leftHand;
        
    }

    private void FixedUpdate()
    {
        rb.velocity = movement;
    }

    public CharacterColor GetCurrentColor()
    {
        return currentColor;
    }
}

[System.Serializable]
public class CharacterSkin
{
    public CharacterColor warna;
    public Sprite body;
    public Sprite rightHand;
    public Sprite leftHand;
}
