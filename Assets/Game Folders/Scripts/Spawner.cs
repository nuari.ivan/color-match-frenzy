using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Shapes prefab;
    [SerializeField] private PowerUp powerUp;

    private void Start()
    {
        GameManager.Instance.OnStateChanged += OnStateChanged;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnStateChanged -= OnStateChanged;
    }

    private void OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                break;
            case GameState.Setting:
                break;
            case GameState.Game:
                StartCoroutine(StartSpawn());
                StartCoroutine(StartSpawnPowerUp());
                break;
            case GameState.Result:
                StopAllCoroutines();
                break;
            case GameState.Exit:
                break;

        }
    }

    IEnumerator StartSpawn()
    {
        yield return new WaitForSeconds(Random.Range(1f, 3f));

        Shapes temp = Instantiate(prefab, transform);
        temp.transform.position = new Vector3(Random.Range(-6f , 6f), 12f, 0f);

        StartCoroutine(StartSpawn());
        
    }

    IEnumerator StartSpawnPowerUp()
    {
        yield return new WaitForSeconds(Random.Range(5f, 10f));

        PowerUp temp = Instantiate(powerUp, transform);
        temp.transform.position = new Vector3(Random.Range(-6f, 6f), 12f, 0f);

        StartCoroutine(StartSpawnPowerUp());
    }
}
