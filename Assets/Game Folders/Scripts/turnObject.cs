using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class turnObject : MonoBehaviour
{
    [SerializeField, Range(-50f, 50f)] private float turnSpeed;
    private void Update()
    {
        transform.Rotate(0f, 0f, Time.deltaTime * turnSpeed);
    }
}