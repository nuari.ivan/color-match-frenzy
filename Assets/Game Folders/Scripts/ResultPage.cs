using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResultPage : Page
{
    [SerializeField] private TMP_Text labelResult;
    [SerializeField] private string[] allResults;

    [SerializeField] private TMP_Text labelScore;
    [SerializeField] private TMP_Text labelHighscore;

    [SerializeField] private Button homeButton;
    [SerializeField] private Button replayButton;
    [SerializeField] private Button exitButton;

    private void OnEnable()
    {
        labelResult.text = $"{allResults[Random.Range(0, allResults.Length)]}";

        int score = GameSetting.Instance.GetScore();
        int highscore = GameManager.Instance.GetHighScore();

        if(score > highscore)
        {
            GameManager.Instance.SetHighscore(score);
        }
        labelScore.text = $"score :<br> <size=65><color=black>{score}</color></size>";
        labelHighscore.text = $"Highscore : <color=black>{GameManager.Instance.GetHighScore()}</color>";
    }

    private void Start()
    {
        homeButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
        exitButton.onClick.AddListener(() => Application.Quit());
        replayButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Game));
    }
}
