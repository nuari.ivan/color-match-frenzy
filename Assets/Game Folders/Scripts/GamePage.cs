using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamePage : Page
{
    [SerializeField] private TMP_Text labelScore;
    [SerializeField] private TMP_Text labelHp;

    [SerializeField] private Image bar;

    [System.Serializable]
    public class PowerUpDetail
    {
        public string nama; 
        public Sprite gambar;
    }

    [SerializeField] private PowerUpDetail[] allDetails;

    [SerializeField] private GameObject powerUpPanel;
    [SerializeField] private Image powerUpIcon;
    [SerializeField] private TMP_Text powerUpLabel;

    private void OnEnable()
    {
        GameSetting.Instance.OnScoreChanged += Instance_OnScoreChanged;
        GameSetting.Instance.OnHpChanged += Instance_OnHpChanged;
        GameSetting.Instance.OnPowerUpActived += Instance_OnPowerUpActived;

        GameSetting.Instance.StartGame();
    }

    private void OnDisable()
    {
        GameSetting.Instance.OnScoreChanged -= Instance_OnScoreChanged;
        GameSetting.Instance.OnHpChanged -= Instance_OnHpChanged;
        GameSetting.Instance.OnPowerUpActived -= Instance_OnPowerUpActived;
    }

    private void Instance_OnPowerUpActived(int powerUpIndex)
    {
        powerUpPanel.SetActive(true);
        powerUpIcon.sprite = allDetails[powerUpIndex].gambar;
        powerUpLabel.text = allDetails[powerUpIndex].nama;

        StartCoroutine(HidePowerUp());
    }

    IEnumerator HidePowerUp()
    {
        yield return new WaitForSeconds(5f);
        powerUpPanel.SetActive(false);
    }

    private void Instance_OnHpChanged(float hp)
    {
        labelHp.text = $"{hp} / 100";
        bar.fillAmount = hp / 100f;
    }

    private void Instance_OnScoreChanged(int newScore)
    {
        labelScore.text = $"SCORE : <br> <size=100><color=green>{newScore}</size></color>";
    }
}