using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HelpPage : Page
{
    [SerializeField] private Button homeButton;

    private void Start()
    {
        homeButton.onClick.AddListener(() =>
        {
            AudioManager.Instance.PlaySound("klik");
            GameManager.Instance.ChangeState(GameState.Menu);
        });
    }
}
