using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetting : MonoBehaviour
{
    public static GameSetting Instance;

    [SerializeField] private int poin;
    [SerializeField] private float maxHp;
    private float currentHp;
    private int modifier = 1;

    public delegate void ScoreChangeDelegate(int newScore);
    public event ScoreChangeDelegate OnScoreChanged;

    public delegate void HpChangeDelegate(float hp);
    public event HpChangeDelegate OnHpChanged;

    public delegate void PowerUpDelegate(int powerUpIndex);
    public event PowerUpDelegate OnPowerUpActived;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        GameManager.Instance.OnStateChanged += Instance_OnStateChanged;
    }
    private void OnDisable()
    {
        GameManager.Instance.OnStateChanged -= Instance_OnStateChanged;
    }

    private void Instance_OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                break;
            case GameState.Setting:
                break;
            case GameState.Game:
                poin = 0;
                currentHp = maxHp;
                
                break;
            case GameState.Pause:
                break;
            case GameState.Exit:
                break;
            case GameState.Result:
                foreach (Transform item in transform)
                {
                    Destroy(item.gameObject);
                }
                break;
            case GameState.Help:
                break;
        }
    }

    public void AddHealth()
    {
        currentHp += 25;
        if(currentHp > 100)
        {
            currentHp = maxHp;
        }
        OnHpChanged?.Invoke(currentHp);
    }

    public void StartGame()
    {
        poin = 0;
        currentHp = maxHp;

        OnHpChanged?.Invoke(currentHp);
        OnScoreChanged?.Invoke(poin);
    }

    public void AddScore()
    {
        poin += modifier;
        OnScoreChanged?.Invoke(poin);
    }

    public void GetHit()
    {
        currentHp -= 10f;
        OnHpChanged?.Invoke(currentHp);
        if(currentHp <=0)
        {
            GameManager.Instance.ChangeState(GameState.Result);
        }
    }

    public void ActivatePowerUp(int n)
    {
        OnPowerUpActived?.Invoke(n);
    }

    public int GetScore()
    {
        return poin;
    }

    public void PowerUpDoubleBonus()
    {
        modifier *= 2;
        StartCoroutine(StopDoubleBonus());
    }

    IEnumerator StopDoubleBonus()
    {
        yield return new WaitForSeconds(5f);
        modifier = 1;
    }
}
