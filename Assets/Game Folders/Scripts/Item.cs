using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField,Range(15f , 150f)] private float turnSpeed;
    protected Rigidbody2D rb;

    protected virtual void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(Gamekey.Player))
        {
            OnDetectPlayer(collision.gameObject);
        }
        if (collision.CompareTag(Gamekey.Ground))
        {
            OnDetectGround();
        }
    }

    protected virtual void OnDetectPlayer(GameObject player)
    {

    }

    protected virtual void OnDetectGround()
    {

    }

    protected void Update()
    {
        transform.Rotate(0f, 0f, Time.deltaTime * turnSpeed);
    }
}



[System.Serializable]
public class ItemSkin
{
    public CharacterColor itemColor;
    public Sprite[] allSkins;
}
