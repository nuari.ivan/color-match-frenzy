using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Item
{
    [SerializeField] private GameObject[] allItem;
    [SerializeField] private GameObject fx;

    private int powerUpIndex;

    private void OnEnable()
    {
        foreach (var item in allItem)
        {
            item.SetActive(false);
        }
        powerUpIndex = Random.Range(0, 3);
        allItem[powerUpIndex].SetActive(true);
    }

    protected override void OnDetectPlayer(GameObject player)
    {
        AudioManager.Instance.PlaySound("Success");
        GameSetting.Instance.ActivatePowerUp(powerUpIndex);

        GameObject tempFx = Instantiate(fx, transform.position, Quaternion.identity);
        Destroy(tempFx, 1.5f);

        Destroy(gameObject);
    }

    protected override void OnDetectGround()
    {
        Destroy(gameObject);
    }
}
