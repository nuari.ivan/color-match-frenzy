public enum GameState
{
    Menu,
    Setting,
    Game,
    Pause,
    Exit,
    Result,
    Help
}

public enum PageName
{
    Menu,
    Setting,
    Game,
    Pause,
    Result,
    Help
}

public enum WidgetName
{
    exit
}

public enum CharacterColor
{
    Red,
    Yellow,
    Blue
}
