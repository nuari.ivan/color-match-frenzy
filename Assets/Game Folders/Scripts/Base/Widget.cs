using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Widget : MonoBehaviour
{
    public WidgetName namaWidget;

    protected void CloseWidget()
    {
        gameObject.SetActive(false);
    }
}
