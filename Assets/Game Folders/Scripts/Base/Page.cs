using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Page : MonoBehaviour
{
    public PageName namaPage;

    protected void PindahScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}
