using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    [SerializeField] private Sound[] allSounds;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);

        InitSounds();
    }

    private void InitSounds()
    {
        foreach (Sound s in allSounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.klip;
            s.source.volume = s.vol;
            s.source.loop = s.isLooping;
        }
    }

    public void PlaySound(string soundName)
    {
        Sound findSound = Array.Find(allSounds, s => s.namaAudio == soundName);
        if(findSound != null)
        {
            findSound.source.Play();
        }
    }

    public void SetBgmToMute(bool ya)
    {
        Sound[] findAll = Array.FindAll(allSounds, b => b.isLooping == true);
        
        foreach (var item in findAll)
        {
            if (ya)
            {
                item.source.volume = 0f;
            }
            else
            {
            item.source.volume = 0.5f;
            }
        }
    }

    public void SetSfxToMute(bool ya)
    {
        Sound[] findAll = Array.FindAll(allSounds, b => b.isLooping == false);

        foreach (var item in findAll)
        {
            if (ya)
            {
                item.source.volume = 0f;
            }
            else
            {
                item.source.volume = 0.8f;
            }
        }
    }
}

[System.Serializable]
public class Sound
{
    public string namaAudio;
    public AudioClip klip;
    public float vol;
    public bool isLooping;

    [HideInInspector]
    public AudioSource source;
}
