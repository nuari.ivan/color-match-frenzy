using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    [SerializeField] private Page[] allPages;
    [SerializeField] private Widget[] allWidgets;

    protected virtual void Start()
    {
        allPages = GetComponentsInChildren<Page>(true);
        allWidgets = GetComponentsInChildren<Widget>(true);

        GameManager.Instance.OnStateChanged += OnStateChanged;
    }

    private void OnDisable()
    {
        GameManager.Instance.OnStateChanged -= OnStateChanged;
    }

    private void OnStateChanged(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                SetPage(PageName.Menu);
                break;
            case GameState.Setting:
                break;
            case GameState.Game:
                SetPage(PageName.Game);
                break;
            case GameState.Result:
                SetPage(PageName.Result);
                break;
            case GameState.Help:
                SetPage(PageName.Help);
                break;
        }
    }

    protected void SetPage(PageName nama)
    {
        foreach (var item in allPages)
        {
            item.gameObject.SetActive(false);
        }
        Page findPage = Array.Find(allPages, p => p.namaPage == nama);
        if(findPage != null)
        {
            findPage.gameObject.SetActive(true);
        }
    }

    protected void ShowWidget(WidgetName nama)
    {
        Widget findWidget = Array.Find(allWidgets, w => w.namaWidget == nama);
        if(findWidget != null)
        {
            findWidget.gameObject.SetActive(true);
        }
    }
}