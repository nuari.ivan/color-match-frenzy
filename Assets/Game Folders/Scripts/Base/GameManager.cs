using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    [SerializeField] private GameState currentState = GameState.Menu;

    [SerializeField] private int highscore;

    public delegate void ChangeStateDelegate(GameState newState);
    public event ChangeStateDelegate OnStateChanged;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public void ChangeState(GameState newState)
    {
        if(newState == currentState)
        {
            return;
        }

        currentState = newState;
        OnStateChanged?.Invoke(currentState);
    }

    public int GetHighScore()
    {
        highscore = PlayerPrefs.GetInt(Gamekey.HighScore, 0);
        return highscore;
    }

    public void SetHighscore(int newScore)
    {
        highscore = newScore;
        PlayerPrefs.SetInt(Gamekey.HighScore, highscore);
    }
}
