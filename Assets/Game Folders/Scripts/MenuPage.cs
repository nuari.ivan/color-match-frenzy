using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button playButton;
    [SerializeField] private Button helpButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private Toggle bgmToggle;
    [SerializeField] private Toggle sfxToggle;

    [SerializeField] private TMP_Text labelHighscore;

    private void Start()
    {
        playButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Game));
        helpButton.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Help));
        exitButton.onClick.AddListener(() => Application.Quit());

        bgmToggle.onValueChanged.AddListener(OnBgmChanged);
        sfxToggle.onValueChanged.AddListener(OnSfxChanged);

        labelHighscore.text = $"Highscore : {GameManager.Instance.GetHighScore()}";
        AudioManager.Instance.PlaySound("bgm");
    }

    private void OnEnable()
    {
        if(GameManager.Instance != null)
        {
            labelHighscore.text = $"Highscore : {GameManager.Instance.GetHighScore()}";
        }

        foreach (Button item in GetComponentsInChildren<Button>())
        {
            item.onClick.AddListener(() => AudioManager.Instance.PlaySound("klik"));
        }
    }

    private void OnSfxChanged(bool ya)
    {
        AudioManager.Instance.SetSfxToMute(!ya);
        AudioManager.Instance.PlaySound("klik");
    }

    private void OnBgmChanged(bool ya)
    {
        AudioManager.Instance.SetBgmToMute(ya);
        AudioManager.Instance.PlaySound("klik");
    }
}
